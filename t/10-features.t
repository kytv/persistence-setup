#!/usr/bin/perl

use strict;
use warnings;

use Test::More;

BEGIN {
    eval 'use Test::BDD::Cucumber::Loader';
    plan skip_all => 'Test::BDD::Cucumber::Loader required' if $@;
}

use FindBin::libs;
use Test::BDD::Cucumber::Loader;
use Test::BDD::Cucumber::Harness::TestBuilder;

my ($executor, @features) = Test::BDD::Cucumber::Loader->load('features/');
my $harness = Test::BDD::Cucumber::Harness::TestBuilder->new({});
$executor->execute( $_, $harness ) for @features;
done_testing;
