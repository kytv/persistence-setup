# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Filip Nyquist <fillerix@fillerix.se>, 2015
# Michael Cavén, 2014
# phst <transifex@sturman.se>, 2014
# leveebreaks, 2014
# WinterFairy <winterfairy@riseup.net>, 2013
# WinterFairy <winterfairy@riseup.net>, 2013
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: Tails developers <tails@boum.org>\n"
"POT-Creation-Date: 2015-08-05 19:02+0200\n"
"PO-Revision-Date: 2015-08-25 19:19+0000\n"
"Last-Translator: Filip Nyquist <fillerix@fillerix.se>\n"
"Language-Team: Swedish (http://www.transifex.com/otf/torproject/language/"
"sv/)\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:48
msgid "Personal Data"
msgstr "Personlig data"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:50
msgid "Keep files stored in the `Persistent' directory"
msgstr "Behåll filer lagrade i katalogen `Persistent'"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:58
msgid "GnuPG"
msgstr "GnuPG"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:60
msgid "GnuPG keyrings and configuration"
msgstr "Nyckelringar och konfiguration för GnuPG"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:68
msgid "SSH Client"
msgstr "SSH klient"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:70
msgid "SSH keys, configuration and known hosts"
msgstr "Nycklar, konfiguration och kända värdar för SSH"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:78
msgid "Pidgin"
msgstr "Pidgin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:80
msgid "Pidgin profiles and OTR keyring"
msgstr "Pidgin profiler och OTR nyckelring"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:88
msgid "Claws Mail"
msgstr "Claws Mail"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:90
msgid "Claws Mail profiles and locally stored email"
msgstr "Claws Mail profiler och lokalt lagrad e-post"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:98
msgid "GNOME Keyring"
msgstr "GNOME Keyring"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:100
msgid "Secrets stored by GNOME Keyring"
msgstr "Lösenord sparade av GNOME Keyring"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:108
msgid "Network Connections"
msgstr "Nätverksanslutningar"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:110
msgid "Configuration of network devices and connections"
msgstr "Konfiguration av nätverksenheter och anslutningar"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:118
msgid "Browser bookmarks"
msgstr "Bokmärken i webbläsaren"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:120
msgid "Bookmarks saved in the Tor Browser"
msgstr "Bokmärken sparade i Tor Browser"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:128
msgid "Printers"
msgstr "Skrivare"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:130
msgid "Printers configuration"
msgstr "Konfiguration av skrivare"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:138
msgid "Bitcoin client"
msgstr "Bitcoin Klienten"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:140
msgid "Electrum's bitcoin wallet and configuration"
msgstr "Electrum's bitcoin plånbok och konfiguration"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:148
msgid "APT Packages"
msgstr "APT-paket"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:150
msgid "Packages downloaded by APT"
msgstr "Paket hämtade av APT"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:158
msgid "APT Lists"
msgstr "APT listor"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:160
msgid "Lists downloaded by APT"
msgstr "Listor hämtade av APT"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:168
msgid "Dotfiles"
msgstr "Punktfiler"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:170
msgid ""
"Symlink into $HOME every file or directory found in the `dotfiles' directory"
msgstr ""
"Länka in i $HOME varje fil och katalog som finns i `dotfiles' katalogen"

#: ../lib/Tails/Persistence/Setup.pm:227
msgid "Setup Tails persistent volume"
msgstr "Konfigurera bestående lagring för Tails"

#: ../lib/Tails/Persistence/Setup.pm:307 ../lib/Tails/Persistence/Setup.pm:451
msgid "Error"
msgstr "Fel"

#: ../lib/Tails/Persistence/Setup.pm:338
#, perl-format
msgid "Device %s already has a persistent volume."
msgstr "Enhet %s innehåller redan en partition med bestående lagring."

#: ../lib/Tails/Persistence/Setup.pm:346
#, perl-format
msgid "Device %s has not enough unallocated space."
msgstr "Enhet %s saknar tillräckligt med opartitionerat utrymme."

#: ../lib/Tails/Persistence/Setup.pm:354 ../lib/Tails/Persistence/Setup.pm:368
#, perl-format
msgid "Device %s has no persistent volume."
msgstr "Enhet %s innehåller ingen partition för bestående lagring."

#: ../lib/Tails/Persistence/Setup.pm:360
msgid ""
"Cannot delete the persistent volume while in use. You should restart Tails "
"without persistence."
msgstr ""
"Partitionen med bestående lagring kan inte tas bort när den används. Starta "
"om Tails utan bestående lagring."

#: ../lib/Tails/Persistence/Setup.pm:379
msgid "Persistence volume is not unlocked."
msgstr "Partitionen med bestående lagring är inte upplåst."

#: ../lib/Tails/Persistence/Setup.pm:384
msgid "Persistence volume is not mounted."
msgstr "Partitionen med bestående lagring är inte monterad."

#: ../lib/Tails/Persistence/Setup.pm:389
msgid "Persistence volume is not readable. Permissions or ownership problems?"
msgstr ""
"Kunde inte läsa ifrån den bestående lagringen. Saknas nödvändiga rättigheter?"

#: ../lib/Tails/Persistence/Setup.pm:394
msgid "Persistence volume is not writable. Maybe it was mounted read-only?"
msgstr ""
"Skrivåtkomst till den bestående lagringen saknas. Kanske den är skrivskyddad?"

#: ../lib/Tails/Persistence/Setup.pm:403
#, perl-format
msgid "Tails is running from non-USB / non-SDIO device %s."
msgstr "Tails körs från icke-USB / icke-SDIO enheten %s."

#: ../lib/Tails/Persistence/Setup.pm:409
#, perl-format
msgid "Device %s is optical."
msgstr "Enheten %s är optisk."

#: ../lib/Tails/Persistence/Setup.pm:416
#, perl-format
msgid "Device %s was not created using Tails Installer."
msgstr "Din enhet %s är inte skapad av Tails installerare."

#: ../lib/Tails/Persistence/Setup.pm:676
msgid "Persistence wizard - Finished"
msgstr "Guiden avslutad"

#: ../lib/Tails/Persistence/Setup.pm:679
msgid ""
"Any changes you have made will only take effect after restarting Tails.\n"
"\n"
"You may now close this application."
msgstr ""
"Ändringar kommer inte att gälla förrän du har startat om Tails.\n"
"\n"
"Du kan nu stänga det här programmet."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:54
msgid "Persistence wizard - Persistent volume creation"
msgstr "Guiden för skapande av bestående lagring"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:57
msgid "Choose a passphrase to protect the persistent volume"
msgstr "Välj ett lösenord att skydda den bestående lagringen med"

#. TRANSLATORS: size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:61
#, perl-format
msgid ""
"A %s persistent volume will be created on the <b>%s %s</b> device. Data on "
"this volume will be stored in an encrypted form protected by a passphrase."
msgstr ""
"En %s partition för bestående lagring kommer att skapas på enheten <b>%s %s</"
"b>. Data på den här partitionen kommer att lagras krypterad skyddad med ett "
"lösenord."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:66
msgid "Create"
msgstr "Skapa"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:110
msgid ""
"<b>Beware!</b> Using persistence has consequences that must be well "
"understood. Tails can't help you if you use it wrong! See <a href='file:///"
"usr/share/doc/tails/website/doc/first_steps/persistence.en.html'>Tails "
"documentation about persistence</a> to learn more."
msgstr ""
"<b>Observera!</b> Att använda bestående lagring medför vissa konsekvenser. "
"Tails kan inte skydda dig om du använder det felaktigt! Läs <a href='file:///"
"usr/share/doc/tails/website/doc/first_steps/persistence.en.html'>Tails "
"dokumentation om bestående lagring</a> för mer info."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:144
msgid "Passphrase:"
msgstr "Lösenord:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:154
msgid "Verify Passphrase:"
msgstr "Upprepa lösenord:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:167
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:231
msgid "Passphrase can't be empty"
msgstr "Lösenordet får inte vara tomt"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:222
msgid "Passphrases do not match"
msgstr "Lösenorden matchar inte"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:262
#: ../lib/Tails/Persistence/Step/Configure.pm:129
msgid "Failed"
msgstr "Misslyckades"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:271
msgid "Mounting Tails persistence partition."
msgstr "Monterar Tails-partition med bestående lagring."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:274
msgid "The Tails persistence partition will be mounted."
msgstr "Tails partition med bestående lagring kommer att monteras."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:283
msgid "Correcting permissions of the persistent volume."
msgstr "Rättar till behörigheterna på den bestående lagringen."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:286
msgid "The permissions of the persistent volume will be corrected."
msgstr "Behörigheterna på den bestående lagringen kommer att rättas till."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:308
msgid "Creating..."
msgstr "Skapar..."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:311
msgid "Creating the persistent volume..."
msgstr "Skapar partition för bestående lagring..."

#: ../lib/Tails/Persistence/Step/Configure.pm:61
msgid "Persistence wizard - Persistent volume configuration"
msgstr "Guiden för konfiguration av bestående lagring"

#: ../lib/Tails/Persistence/Step/Configure.pm:64
msgid "Specify the files that will be saved in the persistent volume"
msgstr "Ange vilka filer som ska sparas i den bestående lagringen"

#. TRANSLATORS: partition, size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Configure.pm:68
#, perl-format
msgid ""
"The selected files will be stored in the encrypted partition %s (%s), on the "
"<b>%s %s</b> device."
msgstr ""
"Valda filer kommer lagras i den krypterade partitionen %s (%s), på enheten "
"<b>%s %s</b>."

#: ../lib/Tails/Persistence/Step/Configure.pm:74
msgid "Save"
msgstr "Spara"

#: ../lib/Tails/Persistence/Step/Configure.pm:143
msgid "Saving..."
msgstr "Sparar..."

#: ../lib/Tails/Persistence/Step/Configure.pm:146
msgid "Saving persistence configuration..."
msgstr "Sparar konfigurationen för bestående lagring..."

#: ../lib/Tails/Persistence/Step/Delete.pm:41
msgid "Persistence wizard - Persistent volume deletion"
msgstr "Guiden för borttagning av bestående lagring"

#: ../lib/Tails/Persistence/Step/Delete.pm:44
msgid "Your persistent data will be deleted."
msgstr "Datan i den bestående lagringen kommer att tas bort."

#: ../lib/Tails/Persistence/Step/Delete.pm:48
#, perl-format
msgid ""
"The persistent volume %s (%s), on the <b>%s %s</b> device, will be deleted."
msgstr ""
"Partitionen %s (%s) med bestående lagring, på enheten <b>%s %s</b>, kommer "
"att tas bort."

#: ../lib/Tails/Persistence/Step/Delete.pm:54
msgid "Delete"
msgstr "Ta bort"

#: ../lib/Tails/Persistence/Step/Delete.pm:101
msgid "Deleting..."
msgstr "Tar bort..."

#: ../lib/Tails/Persistence/Step/Delete.pm:104
msgid "Deleting the persistent volume..."
msgstr "Tar bort den bestående lagringen..."
