# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Tails developers
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# il_doc <filippo.giomi@gmail.com>, 2013
# Francesca Ciceri <madamezou@zouish.org>, 2014
# spawned76 <fswitch20@hotmail.com>, 2012
# HostFat <hostfat@gmail.com>, 2015
# il_doc <filippo.giomi@gmail.com>, 2013
# Friguard <italiano@mcdi.com>, 2014
# jan <jan.reister@unimi.it>, 2012-2013
# Random_R, 2013
# Random_R, 2013
# spawned76 <fswitch20@hotmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: The Tor Project\n"
"Report-Msgid-Bugs-To: Tails developers <tails@boum.org>\n"
"POT-Creation-Date: 2015-08-05 19:02+0200\n"
"PO-Revision-Date: 2015-06-16 18:21+0000\n"
"Last-Translator: HostFat <hostfat@gmail.com>\n"
"Language-Team: Italian (http://www.transifex.com/projects/p/torproject/"
"language/it/)\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:48
msgid "Personal Data"
msgstr "Dati personali"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:50
msgid "Keep files stored in the `Persistent' directory"
msgstr "Mantenere i file in una directory \"Persistent\""

#: ../lib/Tails/Persistence/Configuration/Presets.pm:58
msgid "GnuPG"
msgstr "GnuPG"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:60
msgid "GnuPG keyrings and configuration"
msgstr "Portachiavi GnuPG e configurazione"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:68
msgid "SSH Client"
msgstr "Client SSH"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:70
msgid "SSH keys, configuration and known hosts"
msgstr "Chiavi SSH, configurazione e host conosciuti "

#: ../lib/Tails/Persistence/Configuration/Presets.pm:78
msgid "Pidgin"
msgstr "Pidgin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:80
msgid "Pidgin profiles and OTR keyring"
msgstr "Profili Pidgin e portachiavi OTR"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:88
msgid "Claws Mail"
msgstr "Claws Mail"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:90
msgid "Claws Mail profiles and locally stored email"
msgstr "Profili Claws Mail e email locali"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:98
msgid "GNOME Keyring"
msgstr "Portachiavi GNOME"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:100
msgid "Secrets stored by GNOME Keyring"
msgstr "Segreti contenuti nel portachiavi GNOME"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:108
msgid "Network Connections"
msgstr "Connessioni di rete"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:110
msgid "Configuration of network devices and connections"
msgstr "Configurazione dei dispositivi e delle connessioni di rete"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:118
msgid "Browser bookmarks"
msgstr "Segnalibri del browser"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:120
msgid "Bookmarks saved in the Tor Browser"
msgstr "Segnalibri salvati nel Browser Tor"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:128
msgid "Printers"
msgstr "Stampanti"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:130
msgid "Printers configuration"
msgstr "Configurazione stampanti"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:138
msgid "Bitcoin client"
msgstr "Client Bitcoin"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:140
msgid "Electrum's bitcoin wallet and configuration"
msgstr "Portafoglio bitcoin e configurazione di Electrum"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:148
msgid "APT Packages"
msgstr "Pacchetti APT"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:150
msgid "Packages downloaded by APT"
msgstr "Pacchetti scaricati da APT"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:158
msgid "APT Lists"
msgstr "Liste APT"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:160
msgid "Lists downloaded by APT"
msgstr "Liste scaricate da APT"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:168
msgid "Dotfiles"
msgstr "Dotfiles"

#: ../lib/Tails/Persistence/Configuration/Presets.pm:170
msgid ""
"Symlink into $HOME every file or directory found in the `dotfiles' directory"
msgstr ""
"Crea un symlink in $HOME per ogni file o cartella dentro la cartella "
"\"dotfiles\""

#: ../lib/Tails/Persistence/Setup.pm:227
msgid "Setup Tails persistent volume"
msgstr "Configurare un volume persistente Tails"

#: ../lib/Tails/Persistence/Setup.pm:307 ../lib/Tails/Persistence/Setup.pm:451
msgid "Error"
msgstr "Errore"

#: ../lib/Tails/Persistence/Setup.pm:338
#, perl-format
msgid "Device %s already has a persistent volume."
msgstr "Il dispositivo %s ha già un volume persistente"

#: ../lib/Tails/Persistence/Setup.pm:346
#, perl-format
msgid "Device %s has not enough unallocated space."
msgstr "Il dispositivo %s non ha abbastanza spazio libero"

#: ../lib/Tails/Persistence/Setup.pm:354 ../lib/Tails/Persistence/Setup.pm:368
#, perl-format
msgid "Device %s has no persistent volume."
msgstr "Il dispositivo %s non ha un volume persistente"

#: ../lib/Tails/Persistence/Setup.pm:360
msgid ""
"Cannot delete the persistent volume while in use. You should restart Tails "
"without persistence."
msgstr ""
"Impossibile cancellare un volume persistente mentre è in uso. Dovresti "
"riavviare Tails senza persistenza"

#: ../lib/Tails/Persistence/Setup.pm:379
msgid "Persistence volume is not unlocked."
msgstr "Il volume persistente non è sbloccato"

#: ../lib/Tails/Persistence/Setup.pm:384
msgid "Persistence volume is not mounted."
msgstr "Il volume persistente non è montato"

#: ../lib/Tails/Persistence/Setup.pm:389
msgid "Persistence volume is not readable. Permissions or ownership problems?"
msgstr ""
"Il volume persistente non è leggibile. Problemi con permessi o di proprietà?"

#: ../lib/Tails/Persistence/Setup.pm:394
msgid "Persistence volume is not writable. Maybe it was mounted read-only?"
msgstr ""
"Il volume persistente non è scrivibile. Potrebbe essere montato in sola "
"lettura?"

#: ../lib/Tails/Persistence/Setup.pm:403
#, perl-format
msgid "Tails is running from non-USB / non-SDIO device %s."
msgstr "Tails è in esecuzione dal dispositivo non-USB / non-SDIO %s."

#: ../lib/Tails/Persistence/Setup.pm:409
#, perl-format
msgid "Device %s is optical."
msgstr "Il dispositivo %s è ottico"

#: ../lib/Tails/Persistence/Setup.pm:416
#, perl-format
msgid "Device %s was not created using Tails Installer."
msgstr "Il dispositivo %s non è stato creato mediante l'installatore Tails"

#: ../lib/Tails/Persistence/Setup.pm:676
msgid "Persistence wizard - Finished"
msgstr "Procedura guidata di creazione persistenza completata"

#: ../lib/Tails/Persistence/Setup.pm:679
msgid ""
"Any changes you have made will only take effect after restarting Tails.\n"
"\n"
"You may now close this application."
msgstr ""
"Tutte le modifiche effettuate saranno applicate solo dopo il riavvio di "
"Tails. È ora possibile chiudere l'applicazione."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:54
msgid "Persistence wizard - Persistent volume creation"
msgstr "Procedura guidata: creazione volume persistente"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:57
msgid "Choose a passphrase to protect the persistent volume"
msgstr "Scegliere una frase segreta per proteggere il volume persistente"

#. TRANSLATORS: size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:61
#, perl-format
msgid ""
"A %s persistent volume will be created on the <b>%s %s</b> device. Data on "
"this volume will be stored in an encrypted form protected by a passphrase."
msgstr ""
"Sul dispositivo <b>%s %s</b> verrà creato un volume persistente %s. Su "
"questo volume i dati verranno registrati in forma cifrata protetti da una "
"frase segreta."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:66
msgid "Create"
msgstr "Crea"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:110
msgid ""
"<b>Beware!</b> Using persistence has consequences that must be well "
"understood. Tails can't help you if you use it wrong! See <a href='file:///"
"usr/share/doc/tails/website/doc/first_steps/persistence.en.html'>Tails "
"documentation about persistence</a> to learn more."
msgstr ""
"<b>Attenzione!</b> Usare la persistenza comporta delle conseguenza che vanno "
"valutate. Tails non può fare nulla se vengono usate in maniera sbagliata! "
"Leggere la documentazione <a href='file:///usr/share/doc/tails/website/doc/"
"first_steps/persistence.en.html'>Tails sulla persistenza</a> per ulteriori "
"dettagli."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:144
msgid "Passphrase:"
msgstr "Frase segreta:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:154
msgid "Verify Passphrase:"
msgstr "Verifica la frase segreta:"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:167
#: ../lib/Tails/Persistence/Step/Bootstrap.pm:231
msgid "Passphrase can't be empty"
msgstr "La frase segreta non può essere vuota"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:222
msgid "Passphrases do not match"
msgstr "La frase segreta non corrisponde"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:262
#: ../lib/Tails/Persistence/Step/Configure.pm:129
msgid "Failed"
msgstr "Fallito"

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:271
msgid "Mounting Tails persistence partition."
msgstr "Mount della partizione persistente Tails in corso."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:274
msgid "The Tails persistence partition will be mounted."
msgstr "La partizione persistente di Tails sarà montata."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:283
msgid "Correcting permissions of the persistent volume."
msgstr "Correzione dei permessi del volume persistente."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:286
msgid "The permissions of the persistent volume will be corrected."
msgstr "I permessi del volume persistente verranno corretti."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:308
msgid "Creating..."
msgstr "Creazione in corso..."

#: ../lib/Tails/Persistence/Step/Bootstrap.pm:311
msgid "Creating the persistent volume..."
msgstr "Creazione del volume persistente in corso..."

#: ../lib/Tails/Persistence/Step/Configure.pm:61
msgid "Persistence wizard - Persistent volume configuration"
msgstr "Procedura guidata: configurazione volume persistente"

#: ../lib/Tails/Persistence/Step/Configure.pm:64
msgid "Specify the files that will be saved in the persistent volume"
msgstr "Specificare quali file verranno salvati nella partizione persistente"

#. TRANSLATORS: partition, size, device vendor, device model
#: ../lib/Tails/Persistence/Step/Configure.pm:68
#, perl-format
msgid ""
"The selected files will be stored in the encrypted partition %s (%s), on the "
"<b>%s %s</b> device."
msgstr ""
"Il file selezionato sarà salvato in una partizione criptata %s (%s), sul "
"dispositivo <b>%s %s</b>."

#: ../lib/Tails/Persistence/Step/Configure.pm:74
msgid "Save"
msgstr "Salva"

#: ../lib/Tails/Persistence/Step/Configure.pm:143
msgid "Saving..."
msgstr "Salvataggio in corso..."

#: ../lib/Tails/Persistence/Step/Configure.pm:146
msgid "Saving persistence configuration..."
msgstr "Salvataggio configurazione persistenza in corso..."

#: ../lib/Tails/Persistence/Step/Delete.pm:41
msgid "Persistence wizard - Persistent volume deletion"
msgstr "Procedura guidata: cancellazione volume persistente"

#: ../lib/Tails/Persistence/Step/Delete.pm:44
msgid "Your persistent data will be deleted."
msgstr "I dati persistenti verranno cancellati."

#: ../lib/Tails/Persistence/Step/Delete.pm:48
#, perl-format
msgid ""
"The persistent volume %s (%s), on the <b>%s %s</b> device, will be deleted."
msgstr ""
"Il volume persistente %s (%s), sul dispositivo <b>%s %s</b>, sarà cancellato."

#: ../lib/Tails/Persistence/Step/Delete.pm:54
msgid "Delete"
msgstr "Cancella"

#: ../lib/Tails/Persistence/Step/Delete.pm:101
msgid "Deleting..."
msgstr "Cancellazione in corso..."

#: ../lib/Tails/Persistence/Step/Delete.pm:104
msgid "Deleting the persistent volume..."
msgstr "Cancellazione volume persistente in corso..."
