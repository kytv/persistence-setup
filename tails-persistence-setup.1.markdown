% TAILS-PERSISTENCE-SETUP(1) tails-persistence-setup user manual
% This manual page was written by Tails developers <tails@boum.org>
% February 20, 2012

NAME
====

tails-persistence-setup - Tails persistence setup wizard

SYNOPSIS
========

	tails-persistence-setup [OPTIONS]

OPTIONS
=======

`--help`
--------

Print usage information.

`--verbose`
-----------

Get more output.

`--force`
---------

Make some sanity checks non-fatal.

`--override-liveos-mountpoint`
------------------------------

Mountpoint of the Tails system image.

`--override-boot-device`
------------------------

The UDI of the physical block device where Tails is installed, e.g.
`/org/freedesktop/UDisks/devices/sdb`.

`--override-system-partition`
-----------------------------

The UDI of the partition where Tails is installed, e.g.
`/org/freedesktop/UDisks/devices/sdb1`.

`--step`
--------

Specify once per wizard step to run.

Supported steps are: bootstrap, configure, delete.

Example: `--step bootstrap --step configure`.
