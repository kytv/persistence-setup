=head1 NAME

Tails::Persistence::Role::StatusArea - role to manage a status area

=cut

package Tails::Persistence::Role::StatusArea;
use Moose::Role;
use MooseX::Types::Moose qw( :all );
use MooseX::Has::Sugar::Saccharin;



use 5.10.0;
use namespace::autoclean;
use autodie qw(:all);
use Glib qw{TRUE FALSE};
use Gtk2;


=head1 ATTRIBUTES

=cut

has 'status_area' => lazy_build rw 'Gtk2::HBox';
has 'spinner'     => lazy_build rw 'Gtk2::Spinner';
has 'working'     => lazy_build rw Bool;


=head1 CONSTRUCTORS

=cut

sub _build_status_area {
    my $self = shift;

    my $box = Gtk2::HBox->new(FALSE, 64);
    $box->set_border_width(10);
    $box->pack_start($self->spinner, FALSE, FALSE, 0);

    return $box;
}

sub _build_spinner {
    my $self = shift;

    my $spinner = Gtk2::Spinner->new;
    $spinner->set_size_request(80,80);
    return $spinner;
}


=head1 METHOD MODIFIERS

=cut

after 'working' => sub {
    my $self    = shift;

    return unless @_;
    my $new_value = shift;

    if ($new_value) {
        $self->spinner->start;
        $self->spinner->show;
        $self->go_button->set_sensitive(FALSE);
    }
    else {
        $self->spinner->stop;
        $self->spinner->hide;
    }
};

no Moose 'Role';
1;
