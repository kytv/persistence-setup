=head1 NAME

Tails::Persistence::Role::ConfigurationLine - live-persistence.conf data structure

=cut

package Tails::Persistence::Role::ConfigurationLine;
use Moose::Role;
use MooseX::Method::Signatures;
use MooseX::Types::Moose qw( :all );
use MooseX::Types::Path::Class;
use MooseX::Has::Sugar::Saccharin;



use autodie qw(:all);
use warnings FATAL => 'all';
use Carp;


=head1 ATTRIBUTES

=cut

has 'destination' => lazy_build ro Str;
has 'options' =>
    lazy_build ro 'ArrayRef[Str]',
    traits    => [ 'Array' ],
    handles   => {
        count_options => 'count',
        all_options   => 'elements',
        join_options  => 'join',
        grep_options  => 'grep',
    };


=head1 CONSTRUCTORS

=cut

method _build_options     { return [] }


=head2 new_from_string

input: a line e.g. read from live-persistence.conf

output:
  - undef if that line can be safely ignored (e.g. comment)
  - a new Tails::Persistence::Configuration::Line object if "normal" line
  - throws exception on badly formatted line

=cut
sub new_from_string {
    my $class = shift;
    my $line  = shift;

    chomp $line;

    # skip pure-whitespace lines
    return if $line =~ m{^[[:space:]]*$};
    # skip commented-out lines
    return if $line =~ m{^[[:space:]]*#};

    my ($destination, $options) = split /\s+/, $line;
    defined($destination) or croak "Unparseable line: $line";
    my @options = defined $options ? split(/,/, $options) : ();

    return Tails::Persistence::Configuration::Line->new(
        destination => $destination,
        options     => [ @options ],
    );
}


=head1 METHODS

=cut

=head2 stringify

Returns a in-memory configuration line,
as a string in the live-persistence.conf format,
with no trailing newline.

=cut
method stringify {
    my $out = $self->destination;
    if ($self->count_options) {
        $out .= "\t" . $self->join_options(',');
    }
    return $out;
}

no Moose;
1;
