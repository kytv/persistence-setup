=head1 NAME

Tails::Persistence::Configuration - manage live-persistence.conf and presets

=cut

package Tails::Persistence::Configuration;
use Moose;
use MooseX::Method::Signatures;
use MooseX::Types::Moose qw( :all );
use MooseX::Types::Path::Class;
use MooseX::Has::Sugar::Saccharin;



use autodie qw(:all);
use warnings FATAL => 'all';
use Carp;

use Tails::Persistence::Configuration::Atom;
use Tails::Persistence::Configuration::File;
use Tails::Persistence::Configuration::Presets;

use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("tails-persistence-setup");


=head1 ATTRIBUTES

=cut

has 'config_file_path' => lazy_build rw 'Path::Class::File';
has 'file' =>
    lazy_build rw 'Tails::Persistence::Configuration::File';
has 'presets' =>
    lazy_build rw 'Tails::Persistence::Configuration::Presets';
has 'atoms' =>
    lazy_build rw 'ArrayRef[Tails::Persistence::Configuration::Atom]',
    traits  => [ 'Array' ],
    handles => {
        all_atoms => 'elements',
        push_atom  => 'push',
    };


=head1 CONSTRUCTORS

=cut

method _build_file {
    Tails::Persistence::Configuration::File->new(
        path => $self->config_file_path
    );
}

method _build_presets {
    Tails::Persistence::Configuration::Presets->new();
}

method _build_atoms {
    return $self->merge_file_with_presets($self->file, $self->presets);
}

=head1 METHODS

=cut

method lines_not_in_presets {
    grep {
        my $line = $_;
        ! grep { $_->equals_line($line) } $self->presets->all
    } $self->file->all_lines;
}

method merge_file_with_presets {
    # Modifying and returning clones of the presets atoms would be a bit cleaner.
    $self->presets->set_state_from_lines($self->file->all_lines);

    [
        $self->presets->all,
        map {
            Tails::Persistence::Configuration::Atom->new_from_line(
                $_,
                enabled => 1
            );
        } $self->lines_not_in_presets,
    ];
}

method all_enabled_atoms {
    grep { $_->enabled } $self->all_atoms;
}

method all_enabled_lines {
    map {
        Tails::Persistence::Configuration::Line->new(
            destination => $_->destination,
            options     => $_->options,
        )
    } $self->all_enabled_atoms;
}

method save {
    $self->file->lines([ $self->all_enabled_lines ]);
    $self->file->save;
}

no Moose;
1;
