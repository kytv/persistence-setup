=head1 NAME

Tails::Persistence::Setup - main application class

=cut

package Tails::Persistence::Setup;
use Moose;
use MooseX::Method::Signatures;
use MooseX::Types::Moose qw( :all );
use MooseX::Types::Path::Class;
use MooseX::Has::Sugar::Saccharin;



with 'Tails::Role::DisplayError::Gtk2';
with 'Tails::Role::HasEncoding';
with 'Tails::Role::HasDBus::System';
with 'MooseX::Getopt::Dashes';

use 5.10.0;
use namespace::autoclean;

use autodie qw(:all);
use Carp::Assert::More;
use Data::Dumper;
use English qw{-no_match_vars};
use Glib qw{TRUE FALSE};
use Gtk2 qw{-init};
use Gtk2::Gdk::Keysyms;
use Net::DBus qw(:typing);
use Net::DBus::Annotation qw(:call);
use List::Util qw{max};
use Number::Format qw(:subs);
use Path::Class;
use Try::Tiny;

use Tails::RunningSystem;
use Tails::UDisks;

use Tails::Persistence::Configuration;
use Tails::Persistence::Constants;

use Tails::Persistence::Step::Bootstrap;
use Tails::Persistence::Step::Configure;
use Tails::Persistence::Step::Delete;
use Tails::Persistence::Utils qw{align_up_at_2MiB align_down_at_2MiB step_name_to_class_name get_variable_from_file check_config_file_permissions};

use Locale::gettext;
use POSIX;
setlocale(LC_MESSAGES, "");
textdomain("tails-persistence-setup");


=head1 ATTRIBUTES

=cut

has 'verbose' =>
    ro Bool,
    documentation => q{Get more output.},
    default       => sub {
        exists $ENV{DEBUG} && defined $ENV{DEBUG} && $ENV{DEBUG}
    };

has 'force' =>
    lazy_build ro Bool,
    documentation => q{Make some sanity checks non-fatal.};

has 'udisks' =>
    lazy_build ro 'Tails::UDisks',
    metaclass => 'NoGetopt',
    handles   => [
        qw{device_has_partition_with_label
           device_is_optical device_is_connected_via_a_supported_interface
           device_partition_with_label get_device_property
           partitions udisks_service}
    ];

has 'running_system' =>
    lazy_build ro 'Tails::RunningSystem',
    metaclass => 'NoGetopt',
    handles   => [
        qw{boot_device boot_device_file boot_device_model boot_device_vendor
           boot_device_size started_from_device_installed_with_tails_installer}
    ];

has 'persistence_constants' =>
    lazy_build ro 'Tails::Persistence::Constants',
    metaclass => 'NoGetopt',
    handles   => [
        map {
            "persistence_$_"
        } qw{partition_label partition_guid filesystem_type filesystem_label
             minimum_size filesystem_options state_file}
    ];

has 'main_window' =>
    lazy_build ro 'Gtk2::Window',
    metaclass  => 'NoGetopt';

has "$_" => lazy_build ro Str
    for (qw{override_liveos_mountpoint override_boot_device override_system_partition});

has 'persistence_partition_device_file'=> lazy_build ro Str, metaclass => 'NoGetopt';
has 'persistence_partition_size'     => lazy_build ro Int,  metaclass => 'NoGetopt';
has 'persistence_is_enabled'         => lazy_build ro Bool, metaclass => 'NoGetopt';
has 'persistence_is_read_write'      => lazy_build ro Bool, metaclass => 'NoGetopt';
has 'persistence_partition_mountpoint' => (
    isa        => 'Path::Class::Dir',
    is         => 'rw',
    lazy_build => 1,
    coerce     => 1,
    metaclass  => 'NoGetopt',
);

foreach (qw{beginning_of_free_space size_of_free_space}) {
    has $_ => lazy_build ro Int, metaclass  => 'NoGetopt';
}

has 'current_step' =>
    rw Object,
    predicate 'has_current_step',
    metaclass  => 'NoGetopt';

has 'steps' =>
    lazy_build required ro 'ArrayRef[Str]',
    traits  => ['Array'],
    handles => {
        all_steps       => 'elements',
        number_of_steps => 'count',
        append_to_steps => 'push',
        shift_steps     => 'shift',
        next_step       => 'first',
        grep_steps      => 'grep',
    },
    documentation => q{Specify once per wizard step to run. Supported steps are: bootstrap, configure, delete.};

has 'orig_steps' =>
    rw 'ArrayRef[Str]',
    traits  => ['Array'],
    handles => {
        grep_orig_steps => 'grep',
    };

has 'passphrase' => rw Str, documentation => q{Unsupported. Developers only.};

has 'configuration' =>
    lazy_build rw 'Tails::Persistence::Configuration',
    handles    => { save_configuration => 'save' },
    metaclass  => 'NoGetopt';

has '+codeset'  => ( metaclass => 'NoGetopt' );
has '+encoding' => ( metaclass => 'NoGetopt' );


=head1 CONSTRUCTORS AND BUILDERS

=cut

method BUILD {
    my @orig_steps = $self->all_steps;
    $self->orig_steps(\@orig_steps);
}

sub _build_force {
    my $self = shift;
    0;
}

sub _build_persistence_constants { my $self = shift; Tails::Persistence::Constants->new(); }
sub _build_udisks { my $self = shift; Tails::UDisks->new(); }

sub _build_running_system {
    my $self = shift;

    my @args;
    for (qw{liveos_mountpoint boot_device system_partition}) {
        my $attribute = "override_$_";
        my $predicate = "has_$attribute";
        if ($self->$predicate) {
            push @args, ($_ => $self->$attribute)
        }
    }

    Tails::RunningSystem->new(main_window => $self->main_window, @args);
}

sub _build_persistence_is_enabled {
    my $self = shift;

    -e $self->persistence_state_file || return 0;
    -r $self->persistence_state_file || return 0;

    my $value = $self->get_variable_from_persistence_state_file(
        'TAILS_PERSISTENCE_ENABLED'
    );
    defined($value) && $value eq 'true';
}

sub _build_persistence_is_read_write {
    my $self = shift;

    -e $self->persistence_state_file || return 0;
    -r $self->persistence_state_file || return 0;

    my $value = $self->get_variable_from_persistence_state_file(
        'TAILS_PERSISTENCE_READONLY'
    );
    ! (defined($value) && $value eq 'true');
}

sub _build_steps {
    my $self = shift;

    if ($self->device_has_persistent_volume) {
        return [ qw{configure} ];
    }
    else {
        return [ qw{bootstrap configure} ]
    }
}

sub _build_main_window {
    my $self = shift;
    my $win = Gtk2::Window->new('toplevel');
    $win->set_title($self->encoding->decode(gettext('Setup Tails persistent volume')));

    $win->set_border_width(10);

    $win->add($self->current_step->main_box) if $self->has_current_step;
    $win->signal_connect('destroy' => sub { Gtk2->main_quit; });
    $win->signal_connect('key-press-event' => sub {
        my $twin = shift;
        my $event = shift;
        $win->destroy if $event->keyval == $Gtk2::Gdk::Keysyms{Escape};
    });
    $win->set_default($self->current_step->go_button) if $self->has_current_step;

    return $win;
}

sub _build_persistence_partition_mountpoint {
    my $self = shift;

    my $luks = $self->persistence_partition;
    my $luks_holder = $self->get_device_property($luks, 'LuksHolder');
    my $mountpoints = $self->get_device_property($luks_holder, 'DeviceMountPaths');
    return $mountpoints->[0];
}

sub _build_beginning_of_free_space {
    my $self = shift;

    align_up_at_2MiB(
        max(
            map {
                $self->get_device_property($_, 'PartitionOffset')
              + $self->get_device_property($_, 'PartitionSize')
            } $self->partitions($self->boot_device)
        )
    );
}

sub _build_size_of_free_space {
    my $self = shift;

    align_down_at_2MiB(
        $self->get_device_property($self->boot_device, 'DeviceSize')
      - $self->beginning_of_free_space
  );
}

sub _build_persistence_partition_device_file {
    my $self = shift;

    $self->get_device_property($self->persistence_partition, 'DeviceFilePresentation');
}

sub _build_persistence_partition_size {
    my $self = shift;

    $self->get_device_property($self->persistence_partition, 'PartitionSize');
}

sub _build_configuration {
    my $self = shift;

    my $config_file_path = file($self->persistence_partition_mountpoint, 'persistence.conf');
    if (-e $config_file_path) {
        my $expected_uid = getpwnam('tails-persistence-setup');
        my $expected_gid = getgrnam('tails-persistence-setup');
        try {
            check_config_file_permissions(
                $config_file_path,
                {
                    uid  => $expected_uid,
                    gid  => $expected_gid,
                    mode => oct(600),
                    acl  => '',
                }
            );
        }
        catch {
            $self->display_error(
                $self->main_window,
                $self->encoding->decode(gettext('Error')),
                $self->encoding->decode(gettext($_))
            );
        };
    }

    Tails::Persistence::Configuration->new(
        config_file_path => $config_file_path
    );
}


=head1 METHODS

=cut

sub debug {
    my $self = shift;
    my $mesg = shift;
    say STDERR $self->encoding->encode($mesg) if $self->verbose;
}

sub check_sanity {
    my $self      = shift;
    my $step_name = shift;

    my %step_checks = (
        'bootstrap' => [
            {
                method  => 'device_has_persistent_volume',
                message => $self->encoding->decode(gettext(
                    "Device %s already has a persistent volume.")),
                must_be_false    => 1,
                can_be_forced    => 1,
                needs_device_arg => 1,
            },
            {
                method  => 'device_has_enough_free_space',
                message => $self->encoding->decode(gettext(
                    "Device %s has not enough unallocated space.")),
                needs_device_arg => 1,
            },
        ],
        'delete' => [
            {
                method  => 'device_has_persistent_volume',
                message => $self->encoding->decode(gettext(
                    "Device %s has no persistent volume.")),
                needs_device_arg => 1,
            },
            {
                method  => 'persistence_is_enabled',
                message => $self->encoding->decode(gettext(
                    "Cannot delete the persistent volume while in use. You should restart Tails without persistence.")),
                must_be_false => 1,
            },
        ],
        'configure' => [
            {
                method  => 'device_has_persistent_volume',
                message => $self->encoding->decode(gettext(
                    "Device %s has no persistent volume.")),
                needs_device_arg => 1,
            },
        ],
    );

    if (! $self->grep_orig_steps(sub { $_ eq 'bootstrap' })) {
        push @{$step_checks{configure}}, (
            {
                method  => 'persistence_partition_is_unlocked',
                message => $self->encoding->decode(gettext(
                    "Persistence volume is not unlocked.")),
            },
            {
                method  => 'persistence_filesystem_is_mounted',
                message => $self->encoding->decode(gettext(
                    "Persistence volume is not mounted.")),
            },
            {
                method  => 'persistence_filesystem_is_readable',
                message => $self->encoding->decode(gettext(
                    "Persistence volume is not readable. Permissions or ownership problems?")),
            },
            {
                method  => 'persistence_filesystem_is_writable',
                message => $self->encoding->decode(gettext(
                    "Persistence volume is not writable. Maybe it was mounted read-only?")),
            },
        );
    }

    my @checks = (
        {
            method  => 'device_is_connected_via_a_supported_interface',
            message => $self->encoding->decode(gettext(
                "Tails is running from non-USB / non-SDIO device %s.")),
            needs_device_arg => 1,
        },
        {
            method  => 'device_is_optical',
            message => $self->encoding->decode(gettext(
                "Device %s is optical.")),
            must_be_false    => 1,
            needs_device_arg => 1,
        },
        {
            method  => 'started_from_device_installed_with_tails_installer',
            message => $self->encoding->decode(gettext(
                "Device %s was not created using Tails Installer.")),
            must_be_false => 0,
        },
    );
    if ($step_name
        && exists  $step_checks{$step_name}
        && defined $step_checks{$step_name}
    ) {
        push @checks, @{$step_checks{$step_name}};
    }

    foreach my $check (@checks) {
        my $check_method = $self->meta->get_method($check->{method});
        assert_defined($check_method);
        my $res;
        if (exists($check->{needs_device_arg}) && $check->{needs_device_arg}) {
            $res = $check_method->execute($self, $self->boot_device);
        }
        else {
            $res = $check_method->execute($self);
        }
        if (exists($check->{must_be_false}) && $check->{must_be_false}) {
            $res = ! $res;
        }
        if (! $res) {
            my $message = $self->encoding->decode(sprintf(
                gettext($check->{message}),
                $self->boot_device_file));
            if ($self->force && exists($check->{can_be_forced}) && $check->{can_be_forced}) {
                warn "$message",
                     "... but --force is enabled, ignoring results of this sanity check.";
            }
            else {
                $self->display_error(
                    $self->main_window,
                    $self->encoding->decode(gettext('Error')),
                    $message
                );
                return;
            }
        }
    }

    return 1;
}

sub run {
    my $self = shift;

    $self->debug(sprintf("Working on device %s", $self->boot_device_file));

    # Force initialization in the correct order
    if ($Moose::VERSION >= 2.1) {
        assert_defined($self->configuration);
    }

    $self->main_window->set_visible(FALSE);
    $self->goto_next_step;
    $self->debug("Entering main Gtk2 loop.");
    Gtk2->main;
}

sub device_has_persistent_volume {
    my $self   = shift;
    my $device = shift;
    $device  ||= $self->boot_device;

    return $self->device_has_partition_with_label($device, $self->persistence_partition_label);
}

sub device_has_enough_free_space {
    my $self   = shift;
    my $device = shift;

    $self->size_of_free_space >= $self->persistence_minimum_size;
}

sub persistence_partition {
    my $self = shift;

    $self->device_partition_with_label(
        $self->boot_device,
        $self->persistence_partition_label
    );
}

sub create_persistence_partition {
    my $self = shift;
    (my $opts = shift) ||= {};
    $opts->{async}     ||= 0;
    $opts->{end_cb}    ||= sub { say STDERR "finished." };
    my $passphrase = $opts->{passphrase};

    my $offset    = $self->beginning_of_free_space;
    my $size      = $self->size_of_free_space;
    my $type      = $self->persistence_partition_guid;
    my $label     = $self->persistence_partition_label;
    my $flags     = [];
    my $options   = [];
    my $fstype    = $self->persistence_filesystem_type;
    my $fsoptions = [
        @{$self->persistence_filesystem_options},
        sprintf('luks_encrypt=%s', $passphrase)
    ];

    my $obj = $self->udisks_service->get_object($self->boot_device);

    $self->debug(sprintf(
        "Creating partition of size %s at offset %s on device %s",
        format_bytes($size, mode => "iec"), $offset, $self->boot_device_file
    ));

    if ($opts->{async}) {
        # Net::DBus::RemoteObject's _call_method hardcodes a 60 seconds timeout.
        # Unfortunately, mkfs operations on relatively big USB devices often
        # need more time than this. Therefore, we have to dive into lower level
        # private API's to build a ::PendingCall object with a suitable timeout.
        my $connexion = $self->dbus->get_connection;
        my $timeout = 3600 * 1000;
        my $method_call = $connexion->make_method_call_message(
            $self->udisks_service->get_service_name(),
            $self->boot_device,
            "org.freedesktop.UDisks.Device",
            "PartitionCreate"
        );
        $method_call->append_args_list(
            dbus_uint64($offset),
            dbus_uint64($size),
            $type, $label, $flags, $options, $fstype, $fsoptions
        );
        my $pending_call = $connexion->send_with_reply($method_call, $timeout);
        my $reply = Net::DBus::ASyncReply->_new(pending_call => $pending_call);
        $reply->set_notify($opts->{end_cb});
        $self->debug("waiting...");
    }
    else {
        $obj->as_interface("org.freedesktop.UDisks.Device")
            ->PartitionCreate(
                $offset, $size, $type, $label, $flags, $options,
                $fstype, $fsoptions
            );
        $self->debug("done.");
    }
}

sub delete_persistence_partition {
    my $self = shift;
    (my $opts = shift) ||= {};
    $opts->{async}     ||= 0;
    $opts->{end_cb}    ||= sub { say STDERR "finished." };

    $self->debug(sprintf("Deleting partition %s", $self->persistence_partition_device_file));

    # lock the device if it is unlocked
    my $luksholder = $self->persistence_partition_is_unlocked;
    if ($luksholder) {
        if ($self->persistence_filesystem_is_mounted) {
            $self->udisks_service
                ->get_object($luksholder)
                ->as_interface("org.freedesktop.UDisks.Device")
                ->FilesystemUnmount([])
        }
        $self->lock_luks_device($self->persistence_partition);
    }

    # TODO: wipe the LUKS header (#8436)

    if ($opts->{async}) {
        $self->udisks_service
            ->get_object($self->persistence_partition)
            ->as_interface("org.freedesktop.UDisks.Device")
            ->PartitionDelete(dbus_call_async, [])
            ->set_notify($opts->{end_cb});
        $self->debug("waiting...");
    }
    else {
        $self->udisks_service
            ->get_object($self->persistence_partition)
            ->as_interface("org.freedesktop.UDisks.Device")
            ->PartitionDelete([]);
        $self->debug("done.");
    }
}

sub mount_persistence_partition {
    my $self = shift;
    (my $opts = shift) ||= {};
    $opts->{async}     ||= 0;
    $opts->{end_cb}    ||= sub { say STDERR "finished." };

    $self->debug(sprintf("Mounting partition %s", $self->persistence_partition_device_file));

    my $luks_holder = $self->get_device_property(
        $self->persistence_partition, 'LuksHolder');

    if ($opts->{async}) {
        $self->udisks_service
            ->get_object($luks_holder)
            ->as_interface("org.freedesktop.UDisks.Device")
            ->FilesystemMount(dbus_call_async,
                              $self->persistence_filesystem_type, [])
            ->set_notify($opts->{end_cb});
        $self->debug("waiting...");
    }
    else {
        my $mountpoint = $self->udisks_service
            ->get_object($luks_holder)
            ->as_interface("org.freedesktop.UDisks.Device")
            ->FilesystemMount($self->persistence_filesystem_type, []);
        $self->debug("done.");
        return $mountpoint;
    }
}

sub empty_main_window {
    my $self = shift;

    my $child = $self->main_window->get_child;
    $self->main_window->remove($child) if defined($child);
}

sub run_current_step {
    my $self = shift;
    my ($width, $height) = $self->main_window->get_size();

    $self->debug("Running step " . $self->current_step->name);

    $self->current_step->working(0);
    $self->empty_main_window;
    $self->main_window->add($self->current_step->main_box);
    $self->main_window->set_default($self->current_step->go_button);
    $self->main_window->show_all;
    $self->current_step->working(0);
    $self->main_window->set_visible(TRUE);

    if($self->current_step->name eq 'configure') {
        $self->main_window->resize($width, $self->main_window->get_screen()->get_height());
    }
}

sub goto_next_step {
    my $self = shift;
    (my $opts = shift) ||= {};

    my $next_step;

    if ($next_step = $self->shift_steps) {
        if ($self->check_sanity($next_step)) {
            $self->current_step($self->step_object_from_name($next_step));
            $self->run_current_step;
        }
        else {
            # check_sanity already has displayed an error dialog,
            # that the user already closed.
            exit 2;
        }
    }
    else {
        $self->debug("No more steps.");
        $self->current_step->title->set_text($self->encoding->decode(gettext(
            q{Persistence wizard - Finished}
        )));
        $self->current_step->subtitle->set_text($self->encoding->decode(gettext(
            q{Any changes you have made will only take effect after restarting Tails.

You may now close this application.}
        )));
        $self->current_step->description->set_text(' ');
        $self->current_step->go_button->hide;
        $self->current_step->status_area->hide;
    }
}

sub step_object_from_name {
    my $self = shift;
    my $name = shift;

    my $class_name = step_name_to_class_name($name);

    my %init_args;

    if ($name eq 'bootstrap') {
        %init_args = (
            go_callback => sub {
                $self->create_persistence_partition({ @_ })
            },
            size_of_free_space          => $self->size_of_free_space,
            should_mount_persistence_partition =>
                0 < $self->grep_steps(sub { $_ eq 'configure' }),
            mount_persistence_partition_cb => sub {
                $self->mount_persistence_partition({ @_ })
            },
        );
    }
    elsif ($name eq 'delete') {
        %init_args = (
            go_callback => sub {
                $self->delete_persistence_partition({ @_ })
            },
            persistence_partition      => $self->persistence_partition,
            persistence_partition_device_file => $self->persistence_partition_device_file,
            persistence_partition_size => $self->persistence_partition_size,
        );
    }
    elsif ($name eq 'configure') {
        %init_args = (
            go_callback => sub {
                $self->save_configuration({ @_ })
            },
            configuration              => $self->configuration,
            persistence_partition      => $self->persistence_partition,
            persistence_partition_device_file => $self->persistence_partition_device_file,
            persistence_partition_size => $self->persistence_partition_size,
        );
    }

    return $class_name->new(
        name             => $name,
        encoding         => $self->encoding,
        success_callback => sub { $self->goto_next_step({ @_ }) },
        device_vendor    => $self->boot_device_vendor,
        device_model     => $self->boot_device_model,
        %init_args
    );

}

method get_variable_from_persistence_state_file (Str $variable) {
    get_variable_from_file($self->persistence_state_file, $variable);
}

=head2 persistence_partition_is_unlocked

If locked, return false.
Else, return the LuksHolder.

=cut
method persistence_partition_is_unlocked () {
    my $luksholder = $self->get_device_property(
        $self->persistence_partition, 'LuksHolder'
    );
    # a locked device's LuksHolder is '/'
    return $luksholder if $luksholder ne '/';
    return;
}

method persistence_filesystem_is_mounted () {
    return unless my $luksholder = $self->persistence_partition_is_unlocked;
    $self->get_device_property($luksholder, 'DeviceIsMounted');
}

method persistence_filesystem_is_readable () {
    return unless my $mountpoint = $self->persistence_partition_mountpoint;
    my $ret;
    {
        use filetest 'access'; # take ACLs into account
        $ret = -r $self->persistence_partition_mountpoint;
    }
    return $ret;
}

method persistence_filesystem_is_writable () {
    return unless my $mountpoint = $self->persistence_partition_mountpoint;
    my $ret;
    {
        use filetest 'access'; # take ACLs into account
        $ret = -w $self->persistence_partition_mountpoint;
    }
    return $ret;
}

=head2 lock_luks_device

Try twice, sleeping 10s in between, to lock LUKS device.

Rationale: the dreaded udisks bug:

  org.freedesktop.UDisks.Error.Failed: Error locking luks device:
  timeout (10s) waiting for cleartext device to be removed

=cut
method lock_luks_device ($device) {
    my $failed = 0;

    for (1..3) {
        try {
            $self->udisks_service
                ->get_object($device)
                ->as_interface("org.freedesktop.UDisks.Device")
                ->LuksLock([]);
        }
        catch {
            $failed = 1;
            $self->debug("Locking LUKS device failed: $_");
        };
        last unless $failed;
        sleep 10;
    }

    $failed ? 0 : 1;
}

no Moose;
1;
