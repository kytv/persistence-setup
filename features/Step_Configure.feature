Feature: configure step
  As a Tails user,
  in order to keep some persistent files,
  I want to configure Tails persistence feature.

  Scenario: build a Step::Configure object
    Given I have a Configuration object
    And I have a Step::Configure object
    Then I should have a defined Step::Configure object
    And the list of item buttons should contain 13 elements
    And the list of configuration atoms should contain 13 elements
    And the list box should have 13 children
    And there should be 1 active buttons
    And every active button's atom should be enabled
    And every inactive button's atom should be disabled
    And every active button's checked icon should be displayed
    And every inactive button's checked icon should be hidden

  Scenario: toggling an inactive button on
    Given I have a Configuration object
    And I have a Step::Configure object
    When I toggle an inactive button on
    Then there should be 2 active buttons
    And every active button's atom should be enabled
    And every inactive button's atom should be disabled
    And every active button's checked icon should be displayed
    And every inactive button's checked icon should be hidden

  Scenario: toggling an active button off
    Given I have a Configuration object
    And I have a Step::Configure object
    When I toggle an active button off
    Then there should be 0 active button
    And every active button's atom should be enabled
    And every inactive button's atom should be disabled

  Scenario: click save button
    Given I have a Configuration object
    And I have a Step::Configure object
    When I click the save button
    Then the file should contain 1 line

  Scenario: toggle inactive button on and click save button
    Given I have a Configuration object
    And I have a Step::Configure object
    When I toggle an inactive button on
    And I click the save button
    Then the file should contain 2 lines

  Scenario: toggle active button off and click save button
    Given I have a Configuration object
    And I have a Step::Configure object
    When I toggle an active button off
    And I click the save button
    Then the file should contain 0 line
